package com.elder.modul5;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.elder.modul5.Database.Artikel;
import com.elder.modul5.Database.ArtikelResources;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InputArticleActivity extends AppCompatActivity {
    ArtikelResources dbHelper;
    EditText judul,author,desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupSharedPreferences();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_article);
        dbHelper = new ArtikelResources(getApplicationContext());
        judul = findViewById(R.id.editText);
        author = findViewById(R.id.editText2);
        desc = findViewById(R.id.editText3);

    }

    public void add(View view){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SimpleDateFormat format  = new SimpleDateFormat("dd-MM-yyyy");

        ContentValues values = new ContentValues();
        values.put(ArtikelResources.COLUMN_TITLE,judul.getText().toString());
        values.put(ArtikelResources.COLUMN_AUTHOR,author.getText().toString());
        values.put(ArtikelResources.COLUMN_DESC,desc.getText().toString());
        values.put(ArtikelResources.COLUMN_DATE,format.format(new Date()));

        //Insert Query
        long newRowId = db.insert(ArtikelResources.TABLE_NAME,null,values);
//        Toast.makeText(this, newRowId+"", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }

    private void setupSharedPreferences() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        toggleTheme(prefs.getBoolean("nightMode", false));

    }

    public void toggleTheme(Boolean bo) {
        if (bo) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }

    }
}
